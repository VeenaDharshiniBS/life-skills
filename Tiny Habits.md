# Tiny Habits

## 1. Tiny Habits - BJ Fogg
#### Your takeaways from the video (Minimum 5 points)
- Start with a small habit rather than a major shift.
- After establishing a small habit, celebrate.
- Have a reason why you want to form the habit.
- By making small changes to our everyday routine, we can effect significant change.

## 2. Tiny Habits by BJ Fogg - Core Message
#### Your takeaways from the video in as much detail as possible
- The Tiny Habits approach focuses on quick, simple tasks that you can complete in under a minute.
- By eliminating the prompt, you can stop a behaviour that you don't want to happen.
- Although it's not always simple, taking away the prompt is your best first step to prevent a behaviour from occurring.
- When all three are present, behaviour results:
- Your motivation for engaging in the conduct.
- Ability is the capacity to carry out the action.
- Prompt is your cue to act in a certain way.

#### How can you use B = MAP to make making new habits easier?
- Start with a little habit that is easy for us to complete and takes little drive. 
- Possess habits-triggering behaviours.

#### Why it is important to "Shine" or Celebrate after each successful completion of habit?


## 3. 1% Better Every Day Video
#### Your takeaways from the video (Minimum 5 points)
Getting better by just 1% consistently can build to tremendous improvements, and over time can make a big difference to our success. It's called the principle of 'aggregate marginal gains', and is the idea that if you improve by just 1% consistently, those small gains will add up to remarkable improvement.

## 4. Book Summary of Atomic Habits
#### Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?

#### Write about the book's perspective on how to make a good habit easier?

#### Write about the book's perspective on making a bad habit more difficult?

## 5. Reflection:

#### Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
- Daily solving at-least one problem in coding websites.
- Good and healthy 8 hours of sleep, morning yoga/meditation.

#### Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
- Avoiding social media.
- Avoiding junk.