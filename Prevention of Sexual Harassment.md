# Prevention of Sexual Harassment
### What kinds of behaviour cause sexual harassment?
- Any kind of unwelcome verbal, visual or physical conduct of sexual nature can be sexual harassment.
- Verbal sexual harassment includes comments about dressing, and one's body, gender-based jokes, requesting sexual favors, repeatedly asking a person out, sexual innuendos threats, spreading rumors about a person's personal or sexual life and using foul and obscene language.
- Visual sexual harassment includes posters, drawings, pictures, screen savers, cartoons, emails, or texts of a sexual nature.
- Physical sexual harassment includes sexual assault, impeding or blocking movement inappropriate touching such as kissing, hugging, patting, stroking or rubbing sexual gesturing, or even leering or staring.
- Bullying is also a form of sexual harassment and is hence punishable by law.
Workplace bullying and harassment can occur between co-workers, employers, or from externally like the public, clients, customers, or workers from other organizations.
- It extends to online activities and cyberbullying through email, text messages social networks.

### What would you do in case you face or witness any incident or repeated incidents of such behaviour?
- An unwanted advance is a form of injustice. We need to nip it in the bud, otherwise, the person making the advances might be encouraged.
- If I face such situations I will start by telling the person involved to stop the behavior. Try to be as clear as possible.
- If this does not work, I will put it in writing, tell the person what conduct find offensive, and warn him/her.
- If none of the above works, I will report this issue to the supervisor or a human resource person.
- One should cooperate with any investigation and document all that has happened. Read and understand the organization's sexual harassment policy.
- Whether it’s through protection and support of a person who is being harassed, actively discouraging the harasser, or simply showing that workplace sexual harassment is not acceptable and very everyone should play a vital role.
