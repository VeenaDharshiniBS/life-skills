# Grit and Growth Mindset
## 1. Grit
### Question 1
#### Paraphrase (summarize) the video in a few lines. Use your own words.
IQ varies from person to person. Some may perform well but they may not have stratospheric IQ scores. Those people would have worked hard and long enough to reach that height. Education is understanding and learning from a motivational and psychological perspective. When we take a successful person from any field grit is the only characteristic that emerged as common and it wasn't good looks, health, IQ, or social intelligence. Grit is the passion and perseverance for very long-term goals. Grit gives us the energy to stay for the long term and push ourselves to achieve our goals. Talent doesn't make one gritty it is the growth mindset. Dr. Carol Dweck states when we read and learn about our brain and how it changes and grows in response to the challenge, we are more likely to persevere when we fail because failure is not a permanent condition. So growth mindset is a great idea for building grit. We need to take our best ideas and our strongest intuitions, and we need to test them. If we want to be successful, we have to be willing to fail, to be wrong, and to start over again with lessons learned.

### Question 2
#### What are your key takeaways from the video to take action on?
- Grit gives us the energy to stay for the long term and push ourselves to achieve our goals.
- Talent doesn't make one gritty it is the growth mindset.
- Read and learn about our brain and how it changes and grows in response to the challenge so that we can persevere when we fail.
- Growth mindset is a great idea for building grit.
- We need to take our best ideas and our strongest intuitions, and we need to test them.
- To be successful, be ready to fail, learn from mistakes, and start over again.
***

## 2. Introduction to Growth Mindset
### Question 3
#### Paraphrase (summarize) the video in a few lines in your own words.
1. There are two types of mental states for people.
2. These mindsets are quite important when it comes to learning.
3. While some people have fixed attitudes, others have a growth mindset.
4. People with fixed minds believe that talent and intelligence are predetermined.
5. Individuals with fixed mindsets believe that growth and learning are either impossible or unnecessary.
6. They believe they lack control over their abilities.
7. People with a growth mindset believe that knowledge and abilities may be increased.
8. As a result, people who are good at something are good at it because they learned the talent, and people who are awful at it are bad at it because they weren't willing to put in the work.

### Question 4
#### What are your key takeaways from the video to take action on?
1. Growth-minded people can value what they are doing regardless of the outcome.
2. The growth mentality is the conviction that, with effort, our minds may grow and our intelligence soar.
3. A fixed mindset is the belief that our habits are ingrained in us from birth.
4.  The fixed mentality bases everything on the outcome.
5. If we fall short or aren't the best, it's all for nothing. 
***

## 3. Understanding Internal Locus of Control
### Question 5
#### What is the Internal Locus of Control? What is the key point in the video?
* Internal locus of control: They believed they had entire control over the variables determining their achievements.They reached the conclusion that their focus and extra effort were whatever caused them to execute well on the tasks.We need to think that we are in control of our life and are once again accountable for the things that happen to us if we want to feel motivated all the time.
highly motivated to be able to cope with the many setbacks they face every day. 
* This webinar addressed the need of maintaining a good outlook, being driven, and looking for justifications rather than justifications when carrying out our activities.
The message this film imparted to us was to be doers rather than just explainers and to understand that our largest emotional obstacles to success are more significant than physical ones. 
***

## 4. How to build a Growth Mindset
### Question 6
#### Paraphrase (summarize) the video in a few lines in your own words.
* Trusting on our capacity to deal with problems is the fundamental first premise of life, and it forms the basis of a growth mindset. 
* If we have a difficult aim but are unsure about how to attain it, we must think in a way that will help us to come up with a solution.  
* Second, we must challenge our presumptions and pessimistic attitudes. 
* Thirdly, learning on becoming open to new ideas must really be part of our life curriculum. 
* Respect the struggle because it will make us more resilient and enable us to maintain composure in the face of chaos.

### Question 7
#### What are your key takeaways from the video to take action on?
1. Part of developing a growth mindset is busting the myth that difficulties are unpleasant by nature.
2. Evaluate, take into consideration and understand all of our flaws.
3.  Create clear, doable goals based on our passion and purpose.
4. Recognize issues as great learning chances that we otherwise would not have and accept them.
5. Also, make sure we have enough time in order to beat them. 
***

## 5. Mindset - A MountBlue Warrior Reference Manual
### Question 8
#### What are one or more points that you want to take action on from the manual?
1. I will understand each concept properly.
2. I will stay with a problem till I complete it. I will not quit the problem.
3. I will not leave my code unfinished till I complete the following checklist:
Make it work.
Make it readable.
Make it modular.
Make it efficient.
4. I will treat new concepts and situations as learning opportunities. I will never get pressurized by any concept or situation.
***