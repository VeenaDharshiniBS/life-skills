# Listening and Active Communication
## 1. Active Listening
### Question - 1
#### What are the steps/strategies to do Active Listening?
- Active listening is the act of fully hearing and comprehending the meaning of what someone else is saying.
- Avoid getting distracted by your thoughts.
- Focus on the speaker and topic.
- Try not to interrupt the other person let them finish and then respond.
- Show that you're interested to show with body language if another person is talking.
- Take notes during important conversations.
***

## Reflective Listening
### Question 2
#### According to Fisher's model, what are the key points of Reflective Listening? 
- Hearing and understanding what the other person is communicating through words and body language to the best of your ability.
- Responding to the other person by reflecting the thoughts and feelings you heard in his or
her words, tone of voice, body posture, and gestures.
- It helps the listener understand the message.
- It helps the speaker deliver a clear message.
- It reinforces positive relationships.
- It helps the speaker check the accuracy of their own words and ideas.
- It reduces the odds of two people mistakenly believing they have an understanding.
***

## Reflection
### Question 3
#### What are the obstacles in your listening process?
- Noise - Any outside noise, such as the sound of machinery operating, phones ringing, or other people conversing, can act as a barrier.
- Visual distractions can be as basic as the activity just outside a window or the events occurring within a neighboring office's glass walls.
- Physical setting: Discomforts such as a troubling temperature, inadequate or nonexistent seats, offensive scents, and a separation between the listener and speaker can all be problematic.
- Stress, mental indifference, boredom, cognitive dissonance, and impatience.

### Question 4
#### What can you do to improve your listening?
- Take note of eye contact
- Be attentive but not overly so. 
- Pay attention to nonverbal cues like tone and body language. 
- Visualize what the speaker is saying. 
- IEmpathise with the speaker
- Provide feedback 
- Maintain an open mind.
***

## 4. Types of Communication
### Question 5
#### When do you switch to Passive communication style in your day to day life?
- Maintaining harmony, particularly when dealing with conflict, is a major driver of passive communication.
- I keep quiet when some older individuals advise.

### Question 6
#### When do you switch into Aggressive communication styles in your day to day life?
- When someone speaks in a loud and overbearing voice.
- Criticize me or some persons around me.
- When someone attempts to dominate.
- When someone disrespects my work.
- Saying the same statements repeatedly.

### Question 7
#### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
- Passive-aggressive communication is a pattern of indirectly expressing negative feelings instead of openly addressing them. 
- There’s a disconnect between what a passive-aggressive person says and what he or she does.
- When I procrastinate on work.
- When people ignore me in public.
- Spreading false gossip.

### Question 8
#### How can you make your communication assertive? You can analyse the videos and then think what steps you can apply in your own life?
- Assertive communication is expressing our feelings and needs clearly and honestly while respecting the feelings and needs of others.
- Stand up for yourself.
- Say "no" without feeling guilty.
- Express your wants, needs, and opinions.
- Maintain self-control.
- Judge the situation and be assertive only when it’s appropriate.
***