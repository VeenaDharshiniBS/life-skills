# Focus Management

## What is Deep Work
#### What is Deep Work?
- Deep work is a state of peak concentration that lets you learn hard things and create quality work quickly.
- Deep work is the ability to focus without distraction on a cognitively demanding task.
- Shallow work is non-cognitively demanding, logistical-style work, often performed while distracted.
- Deep work is like a superpower in our increasingly competitive twenty-first-century economy.

## Summary of Deep Work Book
#### Paraphrase all the ideas in the above videos and this one **in detail**.
- Schedule the daily works.
- Set your mind for deep work.
- Take healthy sleep.

#### How can you implement the principles in your day to day life?
- Address your mindset.
- Be aware of where you put your effort.
- Notice how you treat others.
- Establish short-term and long-term goals.
- Time management.

## Dangers of Social Media
#### Your key takeaways from the video
- Reduces Face-to-Face Interaction.
- Increases Cravings for Attention.
- Distracts From Life Goals.
- Can Lead to a Higher Risk of Depression.
- Encountering Cyberbullies.
- Social Comparison Reduces Self-Esteem.