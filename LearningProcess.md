# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### Question 1
### What is the Feynman Technique? Paraphrase the video in your own words.

The method of learning, reviewing, and explaining the concepts in simple words, so that one can spot their faint areas and effectuate learning is termed as Feynman Technique. It is named after Richard Feynman, who was populary called "The Great Explainer" of all times won Nobel Prize in phsics. The best method to make sure you truly comprehend all the minute intricacies of a notion in your mind is to explain it to someone else, or at the very least, pretend you are doing it.

### Question 2
### What are the different ways to implement this technique in your learning process?

It is a four-step process
1. Write the name of the concept at the top of the sheet.
2. Explain the concept in simple words and with some relatable examples, so that it is easy to understand as if you are teaching someone.
3. After your explanation, mark any areas where you were uncertain or sections where you became stopped and needed to stop, then go back to the source material, review your notes, or go through examples until your grasp of these subareas is as firm as your understanding of the other areas.
4. Look over your explanation and try to see any places where you've resorted to using complicated or technical terminology. Then set a challenge for yourself to break down those concepts and explain them in clear, simple terms.

The ultimate test of your understanding of a subject is to describe it as though you were teaching it to someone who didn't have the same base assumptions and base knowledge that you do, which is the key to Feynman technique.

****

## 2. Learning How to Learn TED talk by Barbara Oakley

### Question 3
### Paraphrase the video in detail in your own words.

The video is about how to focus, learn and remember things well.

The brain operation is of two modes focus mode and diffuse mode.
- In focused mode, picture a pinball machine where your ideas bounce off of past occurrences, your prior knowledge, and preexisting ideas.
- In diffuse mode, when a concept is entirely new to you, it feels as though rubber balls are very far apart. As a consequence, when we think, our thoughts range broadly since the balls are far apart, which causes distraction as you can't think attentively.

The most notable people in history, including Thomas Edison and Salvador Dali, have been known to unwind to find solutions to their problems after pulling those ideas back into focus mode.

There are two methods for dealing with procrastination: 
- The first is to push through the pain until it passes. 
- The second method is to divert your focus to feel better. However, doing this might have serious negative effects, thus the Pomodoro technique was developed to help.

The Pomodoro Technique: Set a timer for 25 minutes without any interruptions and work for that entire time with your complete attention. After finishing, have some leisurely enjoyment for a short while. The learning process requires this relaxing to stay focused.

It's a common misconception among students that some of their strongest qualities are also their worst ones. Memory-impaired individuals frequently exhibit greater creativity. Other thoughts frequently slink in because they can't keep these concepts in their heads for very long.

So, there is something called "illusions of competence in learning". Exercise within a matter of a few days can increase our ability to both learn and remember. When you do a homework problem, never just work at once and put it away. Test yourself, and work on that homework problem several times over several days until the solution flows like a song from your mind.

Highlighting important points in pages doesn't go straight to our memory. The most effective technique is simply to look at a page, look away, and see what you can recall. Understanding combined with practice and repetition can you truly gain mastery over what you're learning.

`Don't just follow your passions; broaden your passions, and your life will be enriched beyond measure.`

### Question 4
### What are some of the steps that you can take to improve your learning process?

1. Stay in diffuse mode and once ideas hit move to focussed mode.
2. Avoid procrastination.
3. Pomodoro Technique to learn focussed and relaxed.
4. Exercise things until it gets familiar.
5. Self-examine yourself.
6. Understanding things combined with practice and repetition makes us master.
7. Don't just follow your passions, broaden your passions.

****

## 3. Learn Anything in 20 hours

### Question 5
### Your key takeaways from the video? Paraphrase your understanding.

It takes 10,000 hours to get to the top of an ultra-competitive field, which is equivalent to 5 years of a full-time job. The 10,000 rule was introduced by K. Anders Ericsson, a professor at Florida State University. He figured out all the top people in each field and studied how long it takes to get to the top of those fields. The more deliberately they practice and the more time they spend getting to the top. Likely an author named Malcolm Gladwell in his book "Outliers: The Story of Success" tells the same 10,000 hours rule. 

However, it takes 20 hours to learn a thing from zero if we put deliberate practice into it. It is equivalent to put 45 minutes a day for about a month. There is a way to practice this method intelligently and effectively. 
1. Deconstruct the skill. Break into smaller pieces and decide what is more important, and what parts will help us.
2. Learn enough to self-correct. Get resources about what you learn, but it should not be a key to procrastinating practice.
3. Remove barriers to practice like any kind of distractions. 
4. Practice for at least 20 hours.

The major barrier to learning something new is not intellectual, it's emotional. Feeling stupid doesn't feel good, at the beginning of learning anything new.

### Question 6
### What are some of the steps that you can while approaching a new topic?

While approaching a new topic there is simple 4 step process which takes 20 hours.
1. Deconstruct the skill. Break into smaller pieces and decide what is more important, and what parts will help us.
2. Learn enough to self-correct. Get resources about what you learn, but it should not be a key to procrastinating practice.
3. Remove barriers to practice like any kind of distractions. The major barrier to learning something new is not intellectual, it's emotional. Feeling stupid doesn't feel good, at the beginning of learning anything new.
4. Practice for at least 20 hours.

****