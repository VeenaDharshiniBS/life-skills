# Good Practices for Software Development

### Question 1

#### What is your one major takeaway from each one of the 6 sections. So 6 points in total.

- Always take notes on technical discussions, so that we get a clear idea about the requirement and clarify if we have any doubts.
- Update and communicate our current stage in the project and inform the relevant team members.
- If any questions or doubts communicate properly with clear problem statements through GitHub gists, sandbox environments, or screenshots.
- Make time for our company, our product, and our team members.
- Send updates, queries, or doubts to our team members in a single slack or WhatsApp message. Don't always rely on others to give updates.
- Deep work is very much essential with self relaxation.

### Question 2

#### Which area do you think you need to improve on? What are your ideas to make progress in that area?

- Time management. Doing things before the deadline.
- Getting a solution by a simple and effective approach by considering space and time complexity.
- Taking notes on technical discussions and moving along the team.
