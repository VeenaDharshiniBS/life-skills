Energy Management
1. Manage Energy not Time
Question 1:
What are the activities you do that make you relax - Calm quadrant?
- Drawing
- Movies
- Cooking
- Reading
- Sleeping
- Badminton

Question 2:
When do you find getting into the Stress quadrant?
- We I need to complete multiple tasks at the same time.
- When I feel guilty of wasting time.
- When I learn some new concepts and if it doesn't sits better.


Question 3:
How do you understand if you are in the Excitement quadrant?
- When I forgets all the worries, doesn't think about the next days works and fell less stressed and relaxed.
- When my soul feels happy and light.

4. Sleep is your superpower

Question 4:
Paraphrase the Sleep is your Superpower video in detail.
- Lack of sleep(less than 7 or 8 hours) directly affect the male and female reproductive systems.
- Lack of sleep affects the brain and functions of learning amd memory.
- We need enough sleep before and after study for better memeory.
- Without sleep there is 40% deficit in the function of brain when compared to people with 8 hours sleep.
- It is because of the memory storing site in brain called hippocampus, which got affected in sleep deprived person.
- During the very deepest stages of sleep big powerful brainwaves which are discovered in the study. That shifts memories from short-term storage to permanent long-term storage site within the brain.
- Lack of sleep causes aging, Alzheimer's disease, increase the risk of heart attacks, immune system, cancer, gene activity, cardiovascular disease.
- Avoid naps during day. Regularity is the key point in good and healthy sleep.
- Sleep is a nonnegotiable biological necessity and it is the life-support system.

Question 5:
What are some ideas that you can implement to sleep better?
- Avoid naps during day. 
- Regularity is the key point in good and healthy sleep. We need to wake up at the same time and go to the bed everyday.
- The bedroom temperature of 18 degree celcius is advisable as we likely fall asleep in cold places than hot.