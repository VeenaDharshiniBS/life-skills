# **Introduction**
&ensp;&ensp;&ensp;&ensp;&ensp; &ensp; In this era, 2.5 quintillion bytes of data are generated every day, and data plays a crucial role in decision-making for business operations. It is quite essential to handle data and databases which gives us the need to use a database management system. With various kinds of database management systems present in the market today, the Relational database management system is one of the most popular systems available. This type of database management system uses a structure that allows the users to identify and access data with another piece of data in the database. SQL(Structured Query Language) is the core of a relational database, which is used for storing, accessing, and managing the data in the database.

<img src="https://www.tutorialspoint.com/sql/images/sql-architecture.jpg"
    alt = "Database Architecture"
    style="display: block;
  margin-left: auto;
  margin-right: auto;" />
***

## **Data Types**
Data type | Description
------------- | -------------
CHAR  | A FIXED length string (can contain letters, numbers, and special characters).
VARCHAR | A VARIABLE length string (can contain letters, numbers, and special characters). 
BYTE | 	Equal to CHAR(), but stores binary byte strings.
TEXT | Holds a string with a maximum length of 65,535 bytes
BINARY | Equal to CHAR(), but stores binary byte strings.
BLOB | For BLOBs (Binary Large Objects). Holds up to 65,535 bytes of data
BIT | A bit-value type. The number of bits per value is specified in size.
BOOLEAN / BOOL | Zero is considered false, and nonzero values are considered true.
INT | Signed range is from -2147483648 to 2147483647. The unsigned range is from 0 to 4294967295. The size parameter specifies the maximum display width.
FLOAT | A floating point number. If p is from 0 to 24, the data type becomes FLOAT().
DOUBLE | A normal-size floating point number. If p is from 25 to 53, the data type becomes DOUBLE()
DATE | A date. Format: YYYY-MM-DD. The supported range is from '1000-01-01' to '9999-12-31'
DATETIME | A date and time combination. Format: YYYY-MM-DD hh:mm:ss. The supported range is from '1000-01-01 00:00:00' to '9999-12-31 23:59:59'.
TIMESTAMP | A timestamp. Format: YYYY-MM-DD hh:mm:ss. The supported range is from '1970-01-01 00:00:01' UTC to '2038-01-09 03:14:07' UTC.
YEAR | A year in four-digit format. Values are allowed in four-digit format only.
***

## **SQL Constraints**
&ensp;&ensp;&ensp;&ensp;&ensp; &ensp; SQL constraints are used to specify rules for the data in a table. Constraints are used to limit the type of data that can go into a table. If there is any violation between the constraint and the data action, the action is aborted. Constraints can be column level or table level. \
<br>
The following constraints are commonly used in SQL:
* NOT NULL - Ensures that a column cannot have a NULL value
* UNIQUE - Ensures that all values in a column are different
* PRIMARY KEY - A combination of a NOT NULL and UNIQUE. Uniquely identifies each row in a table
* FOREIGN KEY - Uniquely identifies a row/record in another table
* CHECK - Ensures that all values in a column satisfy a specific condition
* DEFAULT - Sets a default value for a column when no value is specified
* INDEX - Used to create and retrieve data from the database very quickly
***

## **SQL Commands**
&ensp;&ensp;&ensp;&ensp;&ensp; &ensp; SQL commands are instructions that are used to communicate with the database like defining, querying, manipulating, controlling, and transaction of data.

<img src="https://csharpcorner-mindcrackerinc.netdna-ssl.com/article/sql-commands-ddl-dql-dml-dcl-tcl-with-examples/Images/SQL_Diagram.drawio.png"
    alt = "SQL Commands"
    style="display: block;
  margin-left: auto;
  margin-right: auto;" />
***

## DDL Commands
### CREATE
CREATE DB Company

### PRIMARY KEY
The PRIMARY KEY uniquely identifies each record in a table. Primary keys must contain UNIQUE values, and cannot contain NULL values. A table can have only ONE primary key; and in the table, this primary key can consist of single or multiple columns.

### Foreign Key
A FOREIGN KEY is a field in one table, that refers to the PRIMARY KEY in another table.
<br>

CREATE TABLE Person ( \
&ensp;&ensp;&ensp;&ensp;&ensp; &ensp; PersonID INT PRIMARY KEY, \
&ensp;&ensp;&ensp;&ensp;&ensp; &ensp; PersonName varchar(255) NOT NULL, \
);
\
CREATE TABLE Order (\
&ensp;&ensp;&ensp;&ensp;&ensp; &ensp; OrderID INT PRIMARY KEY,\
&ensp;&ensp;&ensp;&ensp;&ensp; &ensp; OrderNumber INT NOT NULL,\
&ensp;&ensp;&ensp;&ensp;&ensp; &ensp; PersonID INT FOREIGN KEY REFERENCES Person(PersonID)\
);

### DROP
DROP DB Company;\
DROP TABLE Person;

### ALTER
ALTER TABLE Person ADD Age INT; \
Here, the age column is added to the Person table.

### TRUNCATE
The TRUNCATE command removes all records from the table retaining the structure of the table.\
TRUNCATE TABLE Person;

## DML Commands
### INSERT
INSERT INTO Person (PersonID, PersonName) VALUES (1, Babu); \
Here PersonID and PersonName have been added whereas the Age attribute is null.

### UPDATE
UPDATE Person SET PersonName = "Anitha" WHERE PersonID = 2; \
The person with PersonID '1' whose PersonName is updated to 'Anitha'.

### DELETE
DELETE FROM Person WHERE PersonID = 2;\
The person with PersonID '2' data is removed.

### SELECT
The SELECT statement is used to select data from a database. Along with SELECT, we can use DISTINCT, COUNT, WHERE clause, BETWEEN AND, NOT IN, ORDER BY, HAVING clause, GROUP BY, TOP, LIMIT, MIN, MAX, COUNT, AVG, SUM, LIKE, AND, OR, etc depending on the requirement of the query.

### WHERE 
The WHERE clause is used to filter records based on some condition.

### ORDER BY
The ORDER BY keyword is used to sort the result set in ascending or descending order.

### GROUP BY
The GROUP BY statement groups rows that have the same values into summary rows.

### HAVING
The HAVING clause was added to SQL because the WHERE keyword cannot be used with aggregate functions.
\
Example:
\
SELECT * \
FROM Person \
WHERE Age IS NOT NULL
GROUP BY Age \
HAVING PersonID>10 \
ORDER BY PersonName ASC;

***

## **JOINS**
JOINS in SQL are commands which are used to combine rows from two or more tables, based on a related column between those tables. They are predominantly used when a user is trying to extract data from tables that have one-to-many or many-to-many relationships between them.

### Types of JOINS
### 1. INNER JOIN
#### The INNER JOIN returns those records which have matching values in both tables.
Example: \
SELECT Order.OrderID, Person.PersonName FROM Order \
INNER JOIN Person ON Order.PersonID = Person.PersonID;

### 2. LEFT JOIN
#### The LEFT JOIN or the LEFT OUTER JOIN returns all the records from the left table and also those records which satisfy a condition from the right table. Also, for the records having no matching values in the right table, the output or the result set will contain the NULL values.
Example: \
SELECT Person.PersonName, Order.OrderID FROM Person \
LEFT JOIN Order ON Person.PersonID = Order.PersonID \
ORDER BY Person.PersonName;

### 3. RIGHT JOIN
#### The RIGHT JOIN or the RIGHT OUTER JOIN returns all the records from the right table and also those records which satisfy a condition from the left table. Also, for the records having no matching values in the left table, the output or the result set will contain the NULL values.
Example: \
SELECT Order.OrderID, Person.PersonName \
FROM Order RIGHT JOIN Person ON Order.PersonID = Person.PersonID \
ORDER BY Order.OrderID;

### 4. FULL JOIN
#### FULL JOIN or FULL OUTER JOIN returns all those records that either have a match in the left(Table1) or the right(Table2) table.
Example: \
SELECT Person.PersonName, Order.OrderID FROM Person \
FULL OUTER JOIN Order ON Person.PersonID = Order.PersonID \
ORDER BY Person.PersonName;

### 5. SELF JOIN
#### SELF JOIN is a joining of a table to itself. This implies that each row in a table is joined with itself.
Example: \
SELECT A.PersonName AS PersonName1, B.PersonName AS PersonName2, A.Age \
FROM Person A, Person B WHERE A.PersonID <> B.PersonID AND A.Age = B.Age \
ORDER BY A.Age;

### 6. CROSS JOIN
#### The CROSS JOIN is a type of join in which a join clause is applied to each row of a table to every row of the other table. Also, when the WHERE condition is used, this type of JOIN behaves as an INNER JOIN, and when the WHERE condition is not present, it behaves like a CARTESIAN product.
Example:\
SELECT p.PersonName, o.OrderNumber\
FROM Person p CROSS JOIN Order o;

<img src="https://cdn.educba.com/academy/wp-content/uploads/2019/11/joins-in-mysql-1.png"
    alt = "Joins"
    style="display: block;
          margin-left: auto;
          margin-right: auto;"
/>
***
## **Aggregate Functions**
- An aggregate function is a function where the values of multiple rows are grouped to form a single value.
- It is used to summarize the data.
- We can also use GROUP BY and DISTINCT clauses with it.
<br>

### Functions
Function | Need
------------- | -------------
SUM | Returns the total value
AVG | Returns the average value
MIN | Returns the smallest value
MAX | Returns the largest value
COUNT | Returns the total number of rows
MEDIAN  | Returns the middle value
***

## References
- https://www.w3schools.com/sql/default.asp
- https://www.youtube.com/watch?v=bLL5NbBEg2I

## Image Links
- https://www.tutorialspoint.com/sql/images/sql-architecture.jpg
- https://csharpcorner-mindcrackerinc.netdna-ssl.com/article/sql-commands-ddl-dql-dml-dcl-tcl-with-examples/Images/SQL_Diagram.drawio.png
- https://cdn.educba.com/academy/wp-content/uploads/2019/11/joins-in-mysql-1.png
***
